﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTower : MonoBehaviour
{
    //Component
    [SerializeField] private SpriteRenderer _towerPlate;
    [SerializeField] private SpriteRenderer _towerHead;

    //Properties
    [SerializeField] private int _shootPower = 1;
    [SerializeField] private float _shootDistance = 1f;
    [SerializeField] private float _shootDelay = 5f;
    [SerializeField] private float _bulletSpeed = 1f;
    [SerializeField] private float _bulletSplashRadius = 0f;
    [SerializeField] private ScBullet _bulletPrefab;


    private float _shootDelayCountdown;
    private ScEnemy _targetEnemy;
    private Quaternion _targetRotation;

    //menyimpan posisi
    public Vector2? PlacePosition { get; private set; }

    public void SetPlacePosition(Vector2? newPosition)
    {
        PlacePosition = newPosition;
    }
    public void LockPlacement()
    {
        transform.position = (Vector2)PlacePosition;
    }

    public void ToggleOrderInLayer(bool toFront)
    {
        int orderInLayer = toFront ? 2 : 0;
        _towerPlate.sortingOrder = orderInLayer;
        _towerHead.sortingOrder = orderInLayer;
    }

    //tower icon get
    public Sprite GetTowerHeadIcon()
    {
        return _towerHead.sprite;
    }

    public void CheckNearestEnemy(List<ScEnemy> enemies)
    {
        if (_targetEnemy != null)//andai ada target.. setelah itu jika targetnya tidak aktif atau kejauhan maka hidup gak ada target
        {
            if (!_targetEnemy.gameObject.activeSelf || Vector3.Distance(transform.position, _targetEnemy.transform.position) > _shootDistance)
            {
                _targetEnemy = null;
            }
            else //kalau masih hidup maka berhenti check
                return;
        }

        //kalau gak ada target ya lanjut cek

        float nearestDistance = Mathf.Infinity;
        ScEnemy nearestEnemy = null;

        foreach (ScEnemy enemy in enemies)
        {
            float tempDistance = Vector3.Distance(transform.position, enemy.transform.position);
            if (tempDistance > _shootDistance)
            {
                continue; //nochance, dia terlalu jauh, gak ada harapan T.T menggapai bidadari... leftSwipe
            }

            if (tempDistance < nearestDistance)
            { //andai pecahkan rekor kedekatan
                nearestDistance = tempDistance;
                nearestEnemy = enemy;
            }

            _targetEnemy = nearestEnemy;

        }


    }

    public void SeekTarget(){
        if(_targetEnemy == null){
            return;
        }
         else{
             Vector3 direction = _targetEnemy.transform.position - transform.position;
             float angle = Mathf.Atan2(direction.y,direction.x) * Mathf.Rad2Deg;
            _targetRotation = Quaternion.AngleAxis(angle - 90,Vector3.forward);
            _towerHead.transform.rotation = Quaternion.RotateTowards(_towerHead.transform.rotation,_targetRotation,Time.deltaTime *180f);

         }
    }

    public void ShootTarget()
    {
        if (_targetEnemy == null)
        {
            return;
        }

        //note kedepannya aku rasa countdown harus berkurang tanpa syarat,, ibarat nya gini, emang kalau gak ada musuh, missileny juga gak di isi ulang/ reload???
        _shootDelayCountdown -= Time.unscaledDeltaTime;
        if (_shootDelayCountdown <= 0f)
        {
            bool headHasAimed = Mathf.Abs(_towerHead.transform.rotation.eulerAngles.z - _targetRotation.eulerAngles.z) < 10f;
            // lol akhirnya aku paham ini apa wkwkwk...
            if (!headHasAimed)
            {
                return;
            }
            //serius gak tau buat apa

            ScBullet bullet = ScLevelManager.Instance.GetBulletFromPool(_bulletPrefab);
            //poolnya dilevel manager.. menurutku mungkin kalau2 ini tower dihapus, gak perlu ngehapus bulletnya juga..
            //karna kalau bullet nya gak dihapus malah ngebloat...              

            bullet.transform.position = transform.position;
            bullet.SetProperties(_shootPower, _bulletSpeed, _bulletSplashRadius);
            bullet.SetTargetEnemy(_targetEnemy);
            bullet.gameObject.SetActive(true);

            _shootDelayCountdown = _shootDelay;

        }
    }



    void OnDrawGizmos()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _shootDistance);
    }




}
