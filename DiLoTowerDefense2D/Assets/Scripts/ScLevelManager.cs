using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ScLevelManager : MonoBehaviour
{
    #region Singlelol
    private static ScLevelManager _instance = null;
    public static ScLevelManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScLevelManager>();
            }
            return _instance;
        }
    }
    #endregion


    [SerializeField] private int _maxLives = 3;
    [SerializeField] private int _totalEnemy = 15;
  
    //UI
    [SerializeField] private GameObject _GameOverPanel;
    [SerializeField] private Text _statusInfo;
    [SerializeField] private Text _livesInfo;
    [SerializeField] private Text _totalEnemyInfo;

    //Gameplay

    private int _currentLives;
    private int _enemyCounter;

    [SerializeField] private Transform _towerUIParent;
    [SerializeField] private GameObject _towerUIPrefab;
    [SerializeField] private ScTower[] _towerPrefabs;
    [SerializeField] private ScEnemy[] _enemyPrefabs;


    [SerializeField] private Transform[] _enemyPaths;
    [SerializeField] private float _spawnDelay = 5f;

    //List of object that need pooling
    private List<ScTower> _spawnedTowers = new List<ScTower>();
    private List<ScEnemy> _spawnedEnemies = new List<ScEnemy>();
    private List<ScBullet> _spawnedBullets = new List<ScBullet>();

    private float _spawnDelayCountdown;
    public bool isOver { get; private set; }


    void Start()
    {
        SetCurrentLives(_maxLives);
        SetTotalEnemy(_totalEnemy);
        InstantiateAllTowerUI();

    }

    private void Update()
    {   //intinya ini kode instantiate enemy belakangan sehingga object langsung active...
        if (Input.GetKeyDown(KeyCode.R))
        {
            Invoke("Retry", 0.5f);
        }

        if (isOver)
        {
            return;
        }
        _spawnDelayCountdown -= Time.unscaledDeltaTime;
        if (_spawnDelayCountdown <= 0f)
        {   
            SpawnEnemy();
            _spawnDelayCountdown = _spawnDelay;
        }

        foreach (ScEnemy enemy in _spawnedEnemies)
        {
            if (!enemy.gameObject.activeSelf)
            {
                continue;
            }
            if (Vector2.Distance(enemy.TargetPosition, (Vector2)enemy.transform.position) < 0.1f)
            {
                enemy.SetCurrentPathIndex(enemy.CurrentPathIndex + 1);
                if (enemy.CurrentPathIndex < _enemyPaths.Length)
                { //kalau belum sampe
                    enemy.SetTargetPosition(_enemyPaths[enemy.CurrentPathIndex].position);
                }
                else
                {
                    enemy.gameObject.SetActive(false);//dah sampe
                    ReduceLives(1);
                }

            }
            else
            { //jadi berhenti satu frame kalau dah sampe lol..
                enemy.MoveToTarget();
            }
        }

        //nembak
        foreach (ScTower tower in _spawnedTowers)
        {
            tower.CheckNearestEnemy(_spawnedEnemies);
            tower.SeekTarget();
            tower.ShootTarget();
        }
    }

    void SetCurrentLives(int curH){
         _currentLives = Mathf.Max(curH,0);
         _livesInfo.text = $"Lives : {_currentLives}";
    }

    void SetTotalEnemy(int totE){
        _enemyCounter = totE;
        _totalEnemyInfo.text = $"Enemy Left : {Mathf.Max(_enemyCounter,0)}";
    }

    public void ReduceLives(int value){
        SetCurrentLives(_currentLives - value);
        if (_currentLives<=0)
        {
            SetGameOver(false);
        }
    }
    void SetGameOver(bool isWin){
        isOver = true;
        _statusInfo.text = isWin? "You Win" : "You Lose";
        _GameOverPanel.gameObject.SetActive(true);
    }

    //Add Pilihan Tower tersedia pada Tower UI
    private void InstantiateAllTowerUI()
    {
        foreach (ScTower tower in _towerPrefabs)
        {
            GameObject newTowerUIObj = Instantiate(_towerUIPrefab.gameObject, _towerUIParent);
            ScTowerUI newTowerUI = newTowerUIObj.GetComponent<ScTowerUI>();
            newTowerUI.SetTowerPrefab(tower);
            newTowerUI.transform.name = tower.name;
        }
    }

    //Add List Tower yg dipakai
    public void RegisterSpawnedTower(ScTower tower)
    {
        _spawnedTowers.Add(tower);
    }

    void SpawnEnemy()
    {   
        SetTotalEnemy(--_enemyCounter);
        if(_enemyCounter < 0){
            bool isAllEnemyDestroyed = _spawnedEnemies.Find(e=> e.gameObject.activeSelf) ==null;
            if (isAllEnemyDestroyed){
                SetGameOver(true);
            }

            return;
        }
        int randomIndex = Random.Range(0, _enemyPrefabs.Length);
        string enemyIndexString = (randomIndex + 1).ToString();
        ScEnemy newEnemy;
        //pooling
        GameObject newEnemyObj = _spawnedEnemies.Find(e => !e.gameObject.activeSelf && e.name.Contains(enemyIndexString))?.gameObject;

        if (newEnemyObj == null)
        {
            newEnemyObj = Instantiate(_enemyPrefabs[randomIndex].gameObject);   //onjectbaru
            newEnemy = newEnemyObj.GetComponent<ScEnemy>();
            _spawnedEnemies.Add(newEnemy);
        }
        else
        {
            newEnemy = newEnemyObj.GetComponent<ScEnemy>(); //objeckDitemukan
        }

        // if (!_spawnedEnemies.Contains(newEnemy))
        // {
        //     _spawnedEnemies.Add(newEnemy);
        // } kayaknya gak perlu lah langsung masukan ke list setelah di instantiate.. apa karna agar getcomponent gak dipanggil dua kali?
        // kenapa gak langsung else aja? terus inisialisi scenemy di luar aja...hmmmm

        newEnemy.transform.position = _enemyPaths[0].position;
        newEnemy.SetTargetPosition(_enemyPaths[1].position);
        newEnemy.SetCurrentPathIndex(1);
        newEnemy.gameObject.SetActive(true);
    }

    public ScBullet GetBulletFromPool(ScBullet bulletType)
    {
        GameObject newBulletObj = _spawnedBullets.Find(b => !b.gameObject.activeSelf && b.name.Contains(bulletType.name))?.gameObject;//tanda tanya jagnan lupa karna mungkin saja null. jadi gak perlu if else gak jelas lagi..
        ScBullet newBullet;

        if (newBulletObj == null)
        {
            newBulletObj = Instantiate(bulletType.gameObject);
            newBullet = newBulletObj.GetComponent<ScBullet>();
            _spawnedBullets.Add(newBullet);
        }
        else { newBullet = newBulletObj.GetComponent<ScBullet>(); }

        return newBullet;

    }

    public void ExplodeAt(Vector3 point, float radius, int power)
    {
        foreach (ScEnemy enemy in _spawnedEnemies)
        {
            if (enemy.gameObject.activeSelf)
            {
                float distance = Vector2.Distance(point, enemy.transform.position);
                if (distance < radius)
                {
                    enemy.ReduceEnemyHealth(power);
                }
            }
        }

    }


    void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


}
