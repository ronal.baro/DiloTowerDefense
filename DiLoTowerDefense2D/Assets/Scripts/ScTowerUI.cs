﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScTowerUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] private Image _towerIcon;

    private ScTower _towerPrefab;
    private ScTower _currentSpawnedTower;

    public void OnBeginDrag(PointerEventData eventData)
    {
        GameObject newTowerObj = Instantiate(_towerPrefab.gameObject);
        _currentSpawnedTower = newTowerObj.GetComponent<ScTower>();
        _currentSpawnedTower.ToggleOrderInLayer(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Camera camera = Camera.main;
        Vector3 mousePosition = Input.mousePosition;
        mousePosition.z = -camera.transform.position.z;
        Vector3 targetPosition = Camera.main.ScreenToWorldPoint(mousePosition);

        _currentSpawnedTower.transform.position = targetPosition;


    }

    public void OnEndDrag(PointerEventData eventData)
    {    if(_currentSpawnedTower.PlacePosition==null){
          Debug.Log(_currentSpawnedTower.PlacePosition);
      
           Destroy(_currentSpawnedTower.gameObject);
        }
        else
        {
            _currentSpawnedTower  .LockPlacement();
            _currentSpawnedTower.ToggleOrderInLayer(false);
            ScLevelManager.Instance.RegisterSpawnedTower(_currentSpawnedTower);
            _currentSpawnedTower =null;
        }
    }

    //setting sprite sesuai tower didalamnya
    public void SetTowerPrefab(ScTower tower)
    {
        _towerPrefab = tower;
        _towerIcon.sprite = tower.GetTowerHeadIcon();
    }

    //
}
