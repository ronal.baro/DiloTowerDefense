﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScTowerPlacement : MonoBehaviour
{
    private ScTower _placedTower;
    private SpriteRenderer _sp;

    private void Start()
    {
        _sp = GetComponent<SpriteRenderer>();

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Tower")
        {
            Debug.Log("SentuhKamu");
            if (_placedTower != null)
            {
                return;
            }
            ScTower tower = other.GetComponent<ScTower>();
            tower.SetPlacePosition(transform.position);
            _placedTower = tower;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {

        Debug.Log("LepasKamu");
        if (_placedTower == null)
        {
            return;
        }

        _placedTower.SetPlacePosition(null);
        _placedTower = null;
    }

}
