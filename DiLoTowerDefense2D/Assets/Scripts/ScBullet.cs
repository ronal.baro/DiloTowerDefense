﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScBullet : MonoBehaviour
{
    private int _bulletPower;
    private float _bulletSpeed;

    private float _bulletSplashRadius;

    private ScEnemy _targetEnemy;

    private void FixedUpdate()
    {
        if (_targetEnemy != null)
        { //target ada
            {
                if (!_targetEnemy.gameObject.activeSelf)
                { // tapi target tidak aktif
                    gameObject.SetActive(false);
                    _targetEnemy = null;
                    return;
                }
            }
            transform.position = Vector3.MoveTowards(transform.position, _targetEnemy.transform.position, _bulletSpeed * Time.fixedDeltaTime);

            //rotate bullet homing missile.. kalau mau terkesan gak homing letakkan saat pertama shoot
            Vector3 direction = _targetEnemy.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        }


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_targetEnemy == null)
        {
            return;

        }

        if (other.gameObject.Equals(_targetEnemy.gameObject))
        {   
            gameObject.SetActive(false);

            //AOE missile
            if (_bulletSplashRadius > 0f)
            {
                ScLevelManager.Instance.ExplodeAt(transform.position,_bulletSplashRadius,_bulletPower);
            }

            //shitty missile
            else
            {
                _targetEnemy.ReduceEnemyHealth(_bulletPower);
            }
            _targetEnemy = null; //biar bisa direcycle


        }

    }

    public void SetProperties(int bulletPower, float bulletSpeed, float bulletSplashRadius)
    {
        _bulletPower = bulletPower;
        _bulletSpeed = bulletSpeed;
        _bulletSplashRadius = bulletSplashRadius;
    }

    public void SetTargetEnemy(ScEnemy enemy)
    {
        _targetEnemy = enemy;
    }
}
