﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScEnemy : MonoBehaviour
{

    [SerializeField] private int _maxHealth = 1;
    [SerializeField] private float _moveSpeed = 1f;
    [SerializeField] private Transform _sprite;
    [SerializeField] private SpriteRenderer _healthbar;
    [SerializeField] private SpriteRenderer _healthfill;

    private int _currentHealth;
    public Vector2 TargetPosition { get; private set; }

    public int CurrentPathIndex { get; private set; }
    private void OnEnable()
    {
        _currentHealth = _maxHealth;
        _healthfill.size = _healthbar.size;
        _sprite.transform.rotation = Quaternion.identity;
    }

    public void MoveToTarget()
    {
        transform.position = Vector2.MoveTowards(transform.position, TargetPosition, _moveSpeed * Time.deltaTime);

        Vector3 moveDirection = TargetPosition - (Vector2)transform.position;
       if (moveDirection != Vector3.zero)
        {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            // _sprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward); //format1
            // _sprite.transform.rotation = Quaternion.Euler(Vector3.forward * angle);  //format2

            //sampai sini rotasi langsung tdak perlahan/animasi

           _sprite.transform.rotation = Quaternion.Slerp(_sprite.transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * 7);
        
       }

    }

    public void SetTargetPosition(Vector2 targetPosition)
    {
        TargetPosition = targetPosition;


    }

    public void SetCurrentPathIndex(int currentIndex)
    {
        CurrentPathIndex = currentIndex;
    }

    public void ReduceEnemyHealth(int damage){
       ScAudioManager.Instance.PlaySFX("hit");
        _currentHealth -= damage;
        if (_currentHealth<=0){
            ScAudioManager.Instance.PlaySFX("die");
            gameObject.SetActive(false);
        }

        float multiplierSisaDarah = (float)_currentHealth/_maxHealth;
        Debug.Log(multiplierSisaDarah);
        _healthfill.size = new Vector2(_healthbar.size.x * multiplierSisaDarah, _healthbar.size.y);

    }

    //Temp
    // private void Update() {
    //      SetTargetPosition(Vector2.one *4);
    //      MoveToTarget();
    // }



}
