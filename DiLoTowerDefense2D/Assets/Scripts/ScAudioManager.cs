﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScAudioManager : MonoBehaviour
{
    #region SingleLol

    private static ScAudioManager _instance = null;
    public static ScAudioManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScAudioManager>();
            }
            return _instance;
        }
    }

    #endregion


    private AudioSource _as;
    [SerializeField] private List<AudioClip> _audioClips;

    private void Start()
    {
        _as = GetComponent<AudioSource>();
    }
    public void PlaySFX(string name)
    {
        AudioClip sfx = _audioClips.Find(a => a.name.Contains(name));
        if (sfx != null)
        {
            _as.PlayOneShot(sfx);
        }
    }
}
